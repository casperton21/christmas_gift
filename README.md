# Chrismas Gift

You only need `docker` and `docker-compose` installed

## Start server

The following command will start the development server at URL http://localhost:8000/:

```bash
docker-compose up -d
```

Install all php dependencies:
```
docker-compose exec php composer install
```

Create database:
```
docker-compose exec php bin/console doctrine:database:create
```

Run migration:
```
docker-compose exec php bin/console doctrine:migrations:migrate --no-interaction
```

# Operations:
Upload a stock file

Request Parameters: "file" **Required**
```
POST /upload
```
---------------------------
Get gifts statistics
```
GET /stocks
```
# Tests suite

Run the command below to launch the tests suite:
```
docker-compose exec php ./vendor/bin/phpunit
```