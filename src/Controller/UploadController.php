<?php

namespace App\Controller;

use App\Entity\Gift;
use App\Entity\Receiver;
use App\Service\Gift\GiftValidator;
use App\Service\Receiver\ReceiverValidator;
use App\Service\Response\ResponseFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

class UploadController extends AbstractController
{
    /**
     * @var GiftValidator
     */
    private GiftValidator $giftValidator;

    /**
     * @var ReceiverValidator
     */
    private ReceiverValidator $receiverValidator;

    public function __construct(GiftValidator $giftValidator, ReceiverValidator $receiverValidator)
    {
        $this->giftValidator = $giftValidator;
        $this->receiverValidator = $receiverValidator;
    }

    /**
     * @Route("/upload", name="upload", methods={"POST"})
     */
    public function uploadFile(Request $request): Response
    {
        $response = new ResponseFactory();

        if (!$request->files->get('file')) {
            $response
                ->setStatusCode(Response::HTTP_BAD_REQUEST)
                ->setMessage(Response::$statusTexts['400'])
                ->setError('File to upload is missing');

        } else {
            $txt_file = file_get_contents($request->files->get('file'));

            $rows = explode("\n", $txt_file);
            array_shift($rows);

            foreach($rows as $row => $data) {
                $row_data = explode(',', $data);

                $gift = new Gift();
                $gift->setUuid((isset($row_data[0]) && Uuid::isValid($row_data[0])) ? $row_data[0] : null);
                $gift->setCode($row_data[1] ?? null);
                $gift->setDescription($row_data[2] ?? null);
                $gift->setPrice((isset($row_data[3]) && is_numeric($row_data[3])) ? $row_data[3] : null);

                $receiver = new Receiver();
                $receiver->setUuid((isset($row_data[4]) && Uuid::isValid($row_data[4])) ? $row_data[4] : null);
                $receiver->setFirstname($row_data[5] ?? null);
                $receiver->setLastname($row_data[6] ?? null);
                $receiver->setCountryCode($row_data[7] ?? null);

                if ($this->giftValidator->hasConstraintsValidationErrors($gift) ||
                    $this->receiverValidator->hasConstraintsValidationErrors($receiver)) {
                    continue;
                }

                $gift->setReceiver($receiver);
                $this->getDoctrine()->getManager()->persist($gift);
            }

            $this->getDoctrine()->getManager()->flush();

            $response
                ->setStatusCode(Response::HTTP_OK)
                ->setMessage(Response::$statusTexts['200']);
        }

        return $response->createJsonResponse();
    }
}
