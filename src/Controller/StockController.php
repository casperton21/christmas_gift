<?php

namespace App\Controller;

use App\Entity\Gift;
use App\Service\Response\ResponseFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StockController extends AbstractController
{
    /**
     * @Route("/stocks", name="stocks_show", methods={"GET"})
     */
    public function showStock(): Response
    {
        $response = new ResponseFactory();

        $statistics = $this->getDoctrine()->getRepository(Gift::class)->getStockStatistics();

        if (empty($statistics)) {
            $response
                ->setStatusCode(Response::HTTP_NO_CONTENT)
                ->setMessage(Response::$statusTexts[204]);
        } else {
            $response
                ->setStatusCode(Response::HTTP_OK)
                ->setMessage(Response::$statusTexts[200])
                ->setData($statistics);
        }

        return $response->createJsonResponse();
    }
}
