<?php

namespace App\Repository;

use App\Entity\Gift;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Gift|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gift|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gift[]    findAll()
 * @method Gift[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GiftRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gift::class);
    }

    /**
     * @return array
     */
    public function getStockStatistics(): array
    {
        $query = $this->_em->createQuery(
            'SELECT AVG(g.price) as averagePrice,
            COUNT(r.country_code) as countryNumber,
            COUNT(g.id) as giftNumber,
            MAX(g.price) as higherPrice,
            MIN(g.price) as lowerPrice
            FROM App\Entity\Gift g JOIN g.receiver r'
        );

        return $query->getResult();
    }
}
