<?php

declare(strict_types=1);

namespace App\Service\Response;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ResponseFactory
{
    private array $data = [];

    private ?string $error = '';

    private ?string $message = '';

    private int $statusCode = Response::HTTP_OK;

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return ResponseFactory
     */
    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getError(): ?string
    {
        return $this->error;
    }

    /**
     * @param string|null $error
     * @return ResponseFactory
     */
    public function setError(?string $error): self
    {
        $this->error = $error;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string|null $message
     * @return ResponseFactory
     */
    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     * @return ResponseFactory
     */
    public function setStatusCode(int $statusCode): self
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function createJsonResponse(array $headers = [], bool $json = false): JsonResponse
    {
        $data = [
            'statusCode' => $this->getStatusCode(),
            'data' => [
                'message' => $this->getMessage(),
                'error' => $this->getError(),
                'data' => $this->getData()
            ]
        ];

        return new JsonResponse($data, $data['statusCode'], $headers, $json);
    }
}