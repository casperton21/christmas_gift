<?php

declare(strict_types=1);

namespace App\Service\Receiver;

use App\Entity\Receiver;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ReceiverValidator
{
    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function hasConstraintsValidationErrors(Receiver $receiver): bool
    {
        $errors = true;

        if ($this->validator->validate($receiver)->count() === 0) {
            $errors = false;
        }

        return $errors;
    }
}
