<?php

declare(strict_types=1);

namespace App\Service\Gift;

use App\Entity\Gift;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GiftValidator
{
    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function hasConstraintsValidationErrors(Gift $gift): bool
    {
        $errors = true;

        if ($this->validator->validate($gift)->count() === 0) {
            $errors = false;
        }

        return $errors;
    }
}
