<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class UploadFileTest extends WebTestCase
{
    public function testUploadAnyFile(): void
    {
        $client = static::createClient();
        $client->request('POST', '/upload');

        $this->assertJson($client->getResponse()->getContent());
        $this->assertSame(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());
    }
}
